package com.example.a2048swipemerge

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.NavHostFragment
import com.example.a2048swipemerge.navigation.AppNavigationHandler
import com.example.a2048swipemerge.utils.launchLifecycle
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.launchIn
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var nav: AppNavigationHandler

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val hostFragment = supportFragmentManager.findFragmentById(R.id.container) as NavHostFragment
        val navController = hostFragment.navController
        nav.buffer.launchLifecycle(lifecycle, lifecycleScope) { it.invoke(navController) }

    }
}