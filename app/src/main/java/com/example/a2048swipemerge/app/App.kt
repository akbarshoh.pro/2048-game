package com.example.a2048swipemerge.app

import android.app.Application
import com.example.a2048swipemerge.data.MyPref
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class App : Application() {
    override fun onCreate() {
        super.onCreate()
        MyPref.init(this)
    }
}