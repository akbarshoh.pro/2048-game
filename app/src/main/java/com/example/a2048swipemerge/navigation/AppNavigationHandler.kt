package com.example.a2048swipemerge.navigation

import kotlinx.coroutines.flow.Flow

interface AppNavigationHandler {
    val buffer: Flow<AppNavigation>
}