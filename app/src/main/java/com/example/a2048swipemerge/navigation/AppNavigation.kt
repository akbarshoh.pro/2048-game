package com.example.a2048swipemerge.navigation

import androidx.navigation.NavController

typealias AppNavigation = NavController.() -> Unit