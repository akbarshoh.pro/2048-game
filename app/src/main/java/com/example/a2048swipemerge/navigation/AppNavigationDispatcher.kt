package com.example.a2048swipemerge.navigation

import androidx.navigation.NavDirections
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.flow.MutableSharedFlow
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AppNavigationDispatcher @Inject constructor() : AppNavigator, AppNavigationHandler {
    override val buffer = MutableSharedFlow<AppNavigation>(replay = 1, onBufferOverflow = BufferOverflow.DROP_LATEST)

    private suspend fun navigate(navigation: AppNavigation) {
        buffer.emit(navigation)
    }
    override suspend fun navigate(directions: NavDirections) = navigate { navigate(directions) }

    override suspend fun navigateUp() = navigate { navigateUp() }
}