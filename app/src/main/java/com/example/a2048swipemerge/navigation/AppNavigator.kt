package com.example.a2048swipemerge.navigation

import androidx.navigation.NavDirections

interface AppNavigator {
    suspend fun navigate(directions: NavDirections)
    suspend fun navigateUp()
}