package com.example.a2048swipemerge.data

enum class SideEnum {
    UP, RIGHT, DOWN, LEFT
}

