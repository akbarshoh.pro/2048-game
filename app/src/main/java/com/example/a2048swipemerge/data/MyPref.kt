package com.example.a2048swipemerge.data

import android.content.Context
import android.content.SharedPreferences
import java.lang.StringBuilder
import javax.inject.Singleton

object MyPref {
    private var pref: SharedPreferences? = null

    fun init(context: Context) {
        if (pref == null)
            pref = context.getSharedPreferences("Game", Context.MODE_PRIVATE)
    }

    fun setMatrix(mtx: ArrayList<ArrayList<Int>>) {
        val str = StringBuilder()
        repeat(mtx.size) { i ->
            repeat(mtx[0].size) { j ->
                str.append(mtx[i][j]).append("#")
            }
        }
        pref!!.edit().putString("mtx", str.toString()).apply()
    }

    fun getMatrix() : ArrayList<ArrayList<Int>> {
        val mtx = arrayListOf(
            arrayListOf(0, 0, 0, 2),
            arrayListOf(0, 0, 2, 0),
            arrayListOf(0, 0, 0, 0),
            arrayListOf(0, 0, 0, 0)
        )
        val str = pref!!.getString("mtx", "")!!
        val arr = if (str == "") arrayListOf<String>() else str.split("#")
        if (str != "") {
            repeat(16) {
                mtx[it / 4][it % 4] = arr[it].toInt()
            }
        }
        return mtx
    }

    fun setScore(score: Int) {
        pref!!.edit().putInt("score", score).apply()
    }

    fun getScore() : Int {
        return pref!!.getInt("score", 0)
    }

    fun setBestScore(score: Int) {
        pref!!.edit().putInt("best", score).apply()
    }

    fun getBestScore() : Int {
        return pref!!.getInt("best", 0)
    }
}