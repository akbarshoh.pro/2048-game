package com.example.a2048swipemerge.di

import com.example.a2048swipemerge.domain.AppRepository
import com.example.a2048swipemerge.domain.AppRepositoryImp
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
interface RepositoryModule {
    @Binds
    fun bindRepository(imp: AppRepositoryImp) : AppRepository
}