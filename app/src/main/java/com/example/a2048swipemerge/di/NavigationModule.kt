package com.example.a2048swipemerge.di

import com.example.a2048swipemerge.navigation.AppNavigationDispatcher
import com.example.a2048swipemerge.navigation.AppNavigationHandler
import com.example.a2048swipemerge.navigation.AppNavigator
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
interface NavigationModule {
    @Binds
    fun bindHandler(nav: AppNavigationDispatcher) : AppNavigationHandler

    @Binds
    fun bindNavigator(nav: AppNavigationDispatcher) : AppNavigator
}