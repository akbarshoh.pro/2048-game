package com.example.a2048swipemerge.domain

interface AppRepository {
    fun leftSwipe()
    fun rightSwipe()
    fun topSwipe()
    fun bottomSwipe()

    fun getMatrix() : ArrayList<ArrayList<Int>>
    fun getScore() : Int
    fun getBestScore() : Int

    fun hasLost(): Boolean
    fun isWin(): Boolean
    fun isFirstWin() : Boolean
    fun setFirstWin()

    fun addElement()
    fun scoreNull()
    fun restartGame()
}