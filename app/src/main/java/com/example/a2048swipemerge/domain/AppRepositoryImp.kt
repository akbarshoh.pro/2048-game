package com.example.a2048swipemerge.domain

import com.example.a2048swipemerge.data.MyPref
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AppRepositoryImp @Inject constructor() : AppRepository {
    private val pref = MyPref

    private var mtx = pref.getMatrix()

    private var gameScore = pref.getScore()
    private var gameBestScore = pref.getBestScore()
    private var isWin = true

    private fun getNewMtx() = arrayListOf(
        arrayListOf(0, 0, 0, 0),
        arrayListOf(0, 0, 0, 0),
        arrayListOf(0, 0, 0, 0),
        arrayListOf(0, 0, 0, 0)
    )

    private fun indexOfZero(): Int {
        val zeros = arrayListOf<Int>()
        for (i in mtx.indices) {
            for (j in mtx[i].indices) {
                if (mtx[i][j] == 0) zeros.add(i * 4 + j)
            }
        }

        if (zeros.isEmpty()) return -1

        zeros.shuffle()
        return zeros[0]
    }

    override fun restartGame() {
        mtx = getNewMtx()
        gameScore = 0
        isWin = true
        addElement()
        addElement()
    }

    override fun addElement() {
        val index = indexOfZero()
        if (index == -1) return
        mtx[index / 4][index % 4] = 2
    }

    override fun scoreNull() {
        gameScore = 0
    }

    override fun leftSwipe() {
        var index: Int
        var canMove = false


        for (i in mtx.indices) {
            index = 0

            for (j in mtx[i].indices) {
                if (mtx[i][j] == 0 || j == 0) {
                    continue
                }

                if (mtx[i][j] == mtx[i][index]) {
                    mtx[i][index] *= 2
                    gameScore += mtx[i][index]
                    canMove = true

                    index++
                    mtx[i][j] = 0
                } else if (mtx[i][index] == 0) {
                    mtx[i][index] = mtx[i][j]
                    mtx[i][j] = 0
                    canMove = true
                } else {
                    mtx[i][++index] = mtx[i][j]

                    if (index != j) {
                        mtx[i][j] = 0
                        canMove = true
                    }
                }
            }
        }

        if (canMove) {
            addElement()
        }
    }


    override fun rightSwipe() {
        var index: Int
        var canMove = false

        for (i in mtx.indices) {
            index = 3

            for (j in mtx[i].size - 1 downTo 0) {
                if (mtx[i][j] == 0 || j == 3) {
                    continue
                }

                if (mtx[i][j] == mtx[i][index]) {
                    mtx[i][index] *= 2
                    gameScore += mtx[i][index] // sum
                    canMove = true

                    index--
                    mtx[i][j] = 0
                } else if (mtx[i][index] == 0) {
                    mtx[i][index] = mtx[i][j]
                    canMove = true

                    mtx[i][j] = 0
                } else {
                    mtx[i][--index] = mtx[i][j]
                    if (index != j) {
                        mtx[i][j] = 0
                        canMove = true
                    }
                }
            }
        }

        if (canMove) {
            addElement()
        }
    }


    override fun topSwipe() {
        var index: Int
        var canMove = false

        for (j in mtx.indices) {
            index = 0
            for (i in mtx[j].indices) {
                if (mtx[i][j] == 0 || i == 0) {
                    continue
                }

                if (mtx[i][j] == mtx[index][j]) {
                    mtx[index][j] *= 2
                    gameScore += mtx[index][j]
                    canMove = true

                    mtx[i][j] = 0
                    index++
                } else if (mtx[index][j] == 0) {
                    mtx[index][j] = mtx[i][j]
                    mtx[i][j] = 0
                    canMove = true

                } else {
                    mtx[++index][j] = mtx[i][j]
                    if (index != i) {
                        mtx[i][j] = 0
                        canMove = true
                    }
                }
            }
        }
        if (canMove) {
            addElement()
        }

    }


    override fun bottomSwipe() {
        var index: Int
        var canMove = false

        for (j in mtx.indices) {
            index = 3

            for (i in mtx.size - 1 downTo 0) {
                if (mtx[i][j] == 0 || i == 3) {
                    continue
                }

                if (mtx[i][j] == mtx[index][j]) {
                    mtx[index][j] *= 2
                    gameScore += mtx[index][j]
                    canMove = true
                    mtx[i][j] = 0
                    index--
                } else if (mtx[index][j] == 0) {
                    mtx[index][j] = mtx[i][j]
                    mtx[i][j] = 0
                    canMove = true

                } else {
                    mtx[--index][j] = mtx[i][j]
                    if (index != i) {
                        mtx[i][j] = 0
                        canMove = true
                    }
                }
            }
        }

        if (canMove) {
            addElement()
        }
    }

    override fun getMatrix(): ArrayList<ArrayList<Int>> = returnNewMatrix(mtx)

    private fun returnNewMatrix(mtx: ArrayList<ArrayList<Int>>): ArrayList<ArrayList<Int>> {
        val newMatrix = ArrayList<ArrayList<Int>>()
        for (row in mtx) {
            val newRow = ArrayList<Int>()
            newRow.addAll(row)
            newMatrix.add(newRow)
        }
        return newMatrix

    }

    override fun getScore(): Int = gameScore

    override fun getBestScore(): Int = if (gameBestScore <= gameScore) gameScore else gameBestScore

    override fun hasLost(): Boolean {
        for (i in mtx.indices) {
            for (j in mtx[i].indices) {
                if (mtx[i][j] == 0) {
                    return false
                }
            }
        }

        for (i in mtx.indices) {
            for (j in mtx[i].indices) {
                val currentValue = mtx[i][j]

                if (j < mtx[i].size - 1 && currentValue == mtx[i][j + 1]) {
                    return false
                }

                if (i < mtx.size - 1 && currentValue == mtx[i + 1][j]) {
                    return false
                }
            }
        }

        return true
    }

    override fun isWin(): Boolean {
        for (i in mtx.indices) {
            for (j in mtx[i].indices)
                if (mtx[i][j] == 2048) {
                    return true
                }
        }
        return false
    }

    override fun isFirstWin(): Boolean = isWin

    override fun setFirstWin() {
        isWin = false
    }
}