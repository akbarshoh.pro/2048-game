package com.example.a2048swipemerge.presentation.screens.about

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.viewpager2.widget.ViewPager2
import com.example.a2048swipemerge.databinding.ScreenAboutBinding
import com.example.a2048swipemerge.presentation.adapters.MyPagerAdapter
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AboutScreen : Fragment() {
    private var _binding: ScreenAboutBinding? = null
    private val binding get() = _binding!!
    private val viewModel: AboutViewModel by viewModels<AboutViewModelImp>()
    private val adapter: MyPagerAdapter by lazy { MyPagerAdapter(childFragmentManager, lifecycle) }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = ScreenAboutBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.pager.adapter = adapter
        binding.wormDotsIndicator.attachTo(binding.pager)

        binding.pager.isUserInputEnabled = false

        binding.pager.registerOnPageChangeCallback(object  : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                when(position) {
                    2 -> {
                        binding.skip.visibility = View.INVISIBLE
                        binding.next.visibility = View.INVISIBLE
                        binding.finish.visibility = View.VISIBLE
                    }
                }
            }
        })

        binding.next.setOnClickListener { binding.pager.currentItem = binding.pager.currentItem + 1 }
        binding.skip.setOnClickListener { viewModel.backToManu() }
        binding.finish.setOnClickListener { viewModel.backToManu() }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}