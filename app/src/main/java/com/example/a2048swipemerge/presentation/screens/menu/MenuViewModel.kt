package com.example.a2048swipemerge.presentation.screens.menu

interface MenuViewModel {
    fun toGameScreen()
    fun toAboutScreen()
    fun toInfoScreen()
}