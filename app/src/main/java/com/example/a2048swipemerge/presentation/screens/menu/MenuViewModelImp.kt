package com.example.a2048swipemerge.presentation.screens.menu

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.a2048swipemerge.domain.AppRepository
import com.example.a2048swipemerge.navigation.AppNavigator
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MenuViewModelImp @Inject constructor(
    private val nav: AppNavigator
) : MenuViewModel, ViewModel() {

    override fun toGameScreen() { viewModelScope.launch { nav.navigate(MenuScreenDirections.actionMenuScreenToGameScreen()) } }

    override fun toAboutScreen() { viewModelScope.launch { nav.navigate(MenuScreenDirections.actionMenuScreenToAboutScreen()) } }

    override fun toInfoScreen() { viewModelScope.launch { nav.navigate(MenuScreenDirections.actionMenuScreenToInfoScreen()) } }

}