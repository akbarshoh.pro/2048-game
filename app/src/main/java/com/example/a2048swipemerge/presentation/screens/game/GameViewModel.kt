package com.example.a2048swipemerge.presentation.screens.game

import kotlinx.coroutines.flow.Flow

interface GameViewModel {
    val dataTransferState: Flow<ArrayList<ArrayList<Int>>>
    val scoreTransferState: Flow<Int>
    val bestScoreTransferState: Flow<Int>
    val isFinishFlow: Flow<Boolean>

    fun loadData()

    fun rightSwipe()
    fun leftSwipe()
    fun topSwipe()
    fun bottomSwipe()

    fun hasLost()
    fun isWin()
    fun restartGame()

    fun toMenu()
}