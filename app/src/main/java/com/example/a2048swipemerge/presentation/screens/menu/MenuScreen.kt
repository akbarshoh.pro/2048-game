package com.example.a2048swipemerge.presentation.screens.menu

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.example.a2048swipemerge.databinding.ScreenMenuBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MenuScreen : Fragment() {
    private var _binding: ScreenMenuBinding? = null
    private val binding get() = _binding!!
    private val viewModel: MenuViewModel by viewModels<MenuViewModelImp>()
    private var time = System.currentTimeMillis()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = ScreenMenuBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.startGame.setOnClickListener {
            if (System.currentTimeMillis() - time > 500) {
                viewModel.toGameScreen()
                time = System.currentTimeMillis()
            }
        }

        binding.howPlay.setOnClickListener {
            if (System.currentTimeMillis() - time > 500) {
                viewModel.toAboutScreen()
                time = System.currentTimeMillis()
            }
        }

        binding.info.setOnClickListener {
            if (System.currentTimeMillis() - time > 500) {
                viewModel.toInfoScreen()
                time = System.currentTimeMillis()
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}