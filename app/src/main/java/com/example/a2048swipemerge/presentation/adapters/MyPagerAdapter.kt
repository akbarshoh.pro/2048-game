package com.example.a2048swipemerge.presentation.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.a2048swipemerge.presentation.screens.about.pages.FirstPage
import com.example.a2048swipemerge.presentation.screens.about.pages.SecondPage
import com.example.a2048swipemerge.presentation.screens.about.pages.ThirdPage

class MyPagerAdapter(fm: FragmentManager, lifecycle: Lifecycle) : FragmentStateAdapter(fm, lifecycle) {

    override fun getItemCount(): Int = 3

    override fun createFragment(position: Int): Fragment = when(position) {
        0 -> FirstPage()
        1 -> SecondPage()
        else -> ThirdPage()
    }
}