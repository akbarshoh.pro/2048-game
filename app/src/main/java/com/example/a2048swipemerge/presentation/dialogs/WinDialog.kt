package com.example.a2048swipemerge.presentation.dialogs

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.example.a2048swipemerge.R
import com.example.a2048swipemerge.databinding.DialogWinBinding


class WinDialog : DialogFragment() {
    private var _binding: DialogWinBinding? = null
    private val binding get() = _binding!!
    private var newGame: (() -> Unit)? = null



    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        view?.setBackgroundColor(Color.TRANSPARENT)
        _binding = DialogWinBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.newGame.setOnClickListener {
            newGame?.invoke()
            dismiss()
        }
        binding.continueGame.setOnClickListener {
            dismiss()
        }
    }

    override fun onStart() {
        super.onStart()
        val window = dialog!!.window
        window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
    }

    fun newGame(block: () -> Unit) {
        newGame = block
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}