package com.example.a2048swipemerge.presentation.screens.splash

interface SplashViewModel {
    suspend fun navigateToMenuScreen()
}