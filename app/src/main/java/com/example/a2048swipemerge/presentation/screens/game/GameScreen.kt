package com.example.a2048swipemerge.presentation.screens.game

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.core.view.get
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.example.a2048swipemerge.data.MyPref
import com.example.a2048swipemerge.data.SideEnum
import com.example.a2048swipemerge.databinding.ScreenGameBinding
import com.example.a2048swipemerge.presentation.dialogs.LoseDialog
import com.example.a2048swipemerge.presentation.dialogs.RestartGameDialog
import com.example.a2048swipemerge.presentation.dialogs.WinDialog
import com.example.a2048swipemerge.utils.MyBackgroundUtil
import com.example.a2048swipemerge.utils.TouchListener
import com.example.a2048swipemerge.utils.launchLifecycle
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class GameScreen : Fragment() {
    private var _binding: ScreenGameBinding? = null
    private val binding get() = _binding!!
    private val viewModel: GameViewModel by viewModels<GameViewModelImp>()
    private val views = ArrayList<AppCompatTextView>(16)
    private var winDialog: WinDialog? = null
    private var loseDialog: LoseDialog? = null
    private var restartGameDialog: RestartGameDialog? = null
    private var time = System.currentTimeMillis()
    private var bestGameScore = 0
    private var gameScore = 0
    private var mtx = arrayListOf<ArrayList<Int>>()
    private val pref = MyPref

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = ScreenGameBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        loadViews()
        viewModel.loadData()
        attachTouchListener()

        viewModel.dataTransferState.launchLifecycle(lifecycle, lifecycleScope) {
            val matrix = it
            mtx = it
            for (i in matrix.indices) {
                for (j in matrix[i].indices) {
                    if (matrix[i][j] == 0) views[i*4 + j].text = ""
                    else views[i*4 + j].text = "${matrix[i][j]}"
                    views[i*4 + j].setBackgroundResource(MyBackgroundUtil.backgroundByAmount(matrix[i][j]))
                }
            }
        }

        viewModel.scoreTransferState.launchLifecycle(lifecycle, lifecycleScope) {
            binding.nowScore.text = it.toString()
            gameScore = it
        }

        viewModel.bestScoreTransferState.launchLifecycle(lifecycle, lifecycleScope) {
            binding.bestScore.text = it.toString()
            bestGameScore = it
        }

        viewModel.isFinishFlow.launchLifecycle(lifecycle, lifecycleScope) {
            if (System.currentTimeMillis() - time > 1000) {
                if (it) {
                    winDialog = WinDialog()
                    winDialog?.show(childFragmentManager, null)
                    winDialog?.newGame {
                        viewModel.restartGame()
                        viewModel.loadData()
                    }
                } else {
                    loseDialog = LoseDialog()
                    loseDialog?.show(childFragmentManager, null)
                    loseDialog?.newGame {
                        viewModel.restartGame()
                        viewModel.loadData()
                    }
                    loseDialog?.backToMenu { viewModel.toMenu() }
                }
                time = System.currentTimeMillis()
            }
        }

        binding.rest.setOnClickListener {
            if (System.currentTimeMillis() - time > 1000) {
                restartGameDialog = RestartGameDialog()
                restartGameDialog?.show(childFragmentManager, null)
                restartGameDialog?.newGame {
                    viewModel.restartGame()
                    viewModel.loadData()
                }
                time = System.currentTimeMillis()
            }
        }

        binding.menu.setOnClickListener { viewModel.toMenu() }
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun attachTouchListener() {
        val myTouchListener = TouchListener(requireContext())
        myTouchListener.setActionSideEnumListener {
            when(it) {
                SideEnum.DOWN -> {
                    viewModel.hasLost()
                    viewModel.isWin()
                    viewModel.bottomSwipe()
                    viewModel.loadData()
                }
                SideEnum.RIGHT -> {
                    viewModel.hasLost()
                    viewModel.isWin()
                    viewModel.rightSwipe()
                    viewModel.loadData()
                }
                SideEnum.UP -> {
                    viewModel.hasLost()
                    viewModel.isWin()
                    viewModel.topSwipe()
                    viewModel.loadData()
                }
                SideEnum.LEFT -> {
                    viewModel.hasLost()
                    viewModel.isWin()
                    viewModel.leftSwipe()
                    viewModel.loadData()
                }
            }
        }
        binding.mainView.setOnTouchListener(myTouchListener)
    }

    private fun loadViews() {
        for (i in 0 until binding.mainView.childCount) {
            val line = binding.mainView[i] as LinearLayoutCompat
            for (j in 0 until line.childCount) {
                views.add(line[j] as AppCompatTextView)
            }
        }
    }

    override fun onPause() {
        super.onPause()
        pref.setBestScore(bestGameScore)
        pref.setMatrix(mtx)
        pref.setScore(gameScore)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}