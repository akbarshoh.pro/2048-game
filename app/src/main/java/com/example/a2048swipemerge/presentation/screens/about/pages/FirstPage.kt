package com.example.a2048swipemerge.presentation.screens.about.pages

import androidx.fragment.app.Fragment
import com.example.a2048swipemerge.R
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class FirstPage : Fragment(R.layout.page_first)