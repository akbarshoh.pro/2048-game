package com.example.a2048swipemerge.presentation.screens.info

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.a2048swipemerge.domain.AppRepository
import com.example.a2048swipemerge.navigation.AppNavigator
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class InfoViewModelImp @Inject constructor(
    private val nav: AppNavigator
) : InfoViewModel, ViewModel() {

    override fun backToMenu() { viewModelScope.launch { nav.navigateUp() } }

}