package com.example.a2048swipemerge.presentation.screens.about.pages

import androidx.fragment.app.Fragment
import com.example.a2048swipemerge.R

class SecondPage : Fragment(R.layout.page_second)