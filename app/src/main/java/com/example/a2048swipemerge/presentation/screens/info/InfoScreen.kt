package com.example.a2048swipemerge.presentation.screens.info

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.example.a2048swipemerge.databinding.ScreenInfoBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class InfoScreen : Fragment() {
    private var _binding: ScreenInfoBinding? = null
    private val binding get() = _binding!!
    private val viewModel: InfoViewModel by viewModels<InfoViewModelImp>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = ScreenInfoBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.backToMenu.setOnClickListener { viewModel.backToMenu() }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}