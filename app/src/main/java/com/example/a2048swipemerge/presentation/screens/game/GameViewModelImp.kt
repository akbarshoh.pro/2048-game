package com.example.a2048swipemerge.presentation.screens.game

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.a2048swipemerge.domain.AppRepository
import com.example.a2048swipemerge.navigation.AppNavigator
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class GameViewModelImp @Inject constructor(
    private val repo: AppRepository,
    private val nav: AppNavigator
) : GameViewModel, ViewModel() {
    override val dataTransferState = MutableStateFlow<ArrayList<ArrayList<Int>>>(arrayListOf())
    override val scoreTransferState = MutableStateFlow(0)
    override val bestScoreTransferState = MutableStateFlow(0)
    override val isFinishFlow = MutableSharedFlow<Boolean>(replay = 1, onBufferOverflow = BufferOverflow.DROP_LATEST)

    override fun loadData() {
        viewModelScope.launch {
            dataTransferState.emit(repo.getMatrix())
            scoreTransferState.emit(repo.getScore())
            bestScoreTransferState.emit(repo.getBestScore())
        }
    }

    override fun rightSwipe() = repo.rightSwipe()

    override fun leftSwipe() = repo.leftSwipe()

    override fun topSwipe() = repo.topSwipe()

    override fun bottomSwipe() = repo.bottomSwipe()

    override fun hasLost() {
        val lose = repo.hasLost()
        viewModelScope.launch {
            if (lose)
                isFinishFlow.emit(false)
        }
    }

    override fun isWin() {
        val win = repo.isWin()
        viewModelScope.launch {
            if (win && repo.isFirstWin()) {
                isFinishFlow.emit(win)
                repo.setFirstWin()
            }

        }
    }

    override fun restartGame() = repo.restartGame()

    override fun toMenu() { viewModelScope.launch { nav.navigateUp() } }

}