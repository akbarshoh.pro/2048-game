package com.example.a2048swipemerge.presentation.screens.splash

import androidx.lifecycle.ViewModel
import com.example.a2048swipemerge.navigation.AppNavigator
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import javax.inject.Inject

@HiltViewModel
class SplashViewModelImp @Inject constructor(
    private val nav: AppNavigator
) : SplashViewModel, ViewModel() {

    override suspend fun navigateToMenuScreen() {
        delay(1000)
        nav.navigate(SplashScreenDirections.actionSplashScreenToMenuScreen())
    }

}