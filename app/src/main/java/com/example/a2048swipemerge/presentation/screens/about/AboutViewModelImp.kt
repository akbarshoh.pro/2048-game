package com.example.a2048swipemerge.presentation.screens.about

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.a2048swipemerge.navigation.AppNavigator
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AboutViewModelImp @Inject constructor(
    private val nav: AppNavigator
) : AboutViewModel, ViewModel() {

    override fun backToManu() { viewModelScope.launch { nav.navigateUp() } }

}